#!/bin/sh

FROM python:alpine3.7

RUN apt-get update -y
RUN apt-get install libmariadb3 libmariadb-dev sqlite3 libsqlite3-dev -y

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app
RUN pip3 install -r requirements.txt
COPY . /app

ENTRYPOINT [ "python" ]
CMD [ "run.py" ]
